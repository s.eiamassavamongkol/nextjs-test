"use client";
import ListMenuTest from "@/components/ListMenuTest";
import React from "react";
import Button from "@mui/material/Button";
import CatFacts from "@/components/CatTest";

export default function Home() {

  return (
    <main>
      <ListMenuTest>
        <CatFacts />
      </ListMenuTest>
    </main>
  );
}
