import React, { FC, useEffect, useState } from 'react';
import { styled, Theme, CSSObject } from '@mui/material/styles';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
// import { useLocation, useNavigate } from 'react-router-dom';
import { AppBar, Box, Hidden, Toolbar, useMediaQuery, useTheme, Typography } from '@mui/material';
// import { makeStyles } from '@mui/styles';
import NotesIcon from '@mui/icons-material/Notes';
import { useDispatch } from 'react-redux';
import { AcnColorTheme } from '../theme';
import ListMenu from './ListMenu';
import logo from '../../assets/svg/leftmenu_logo.svg';
import { AcnAppVersion } from '../footer';
import { checkAuthPath } from '../../utils/CheckAuthPath';
import { updateIsLeftMenuOpen } from '../../store/common/CommonSlice';

const openedMixin = (theme: Theme): CSSObject => ({
  width: theme.spacing(46),
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
});

const closedMixin = (theme: Theme): CSSObject => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'hidden',
  width: theme.spacing(16),
  [theme.breakpoints.up('sm')]: {
    width: theme.spacing(16),
  },
});

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(5, 4, 0),
}));

const StyledDrawer = styled(Drawer, { shouldForwardProp: (prop) => prop !== 'open' })(({ theme, open }) => ({
  width: theme.spacing(46),
  flexShrink: 0,
  whiteSpace: 'nowrap',
  boxSizing: 'border-box',
  ...(open && {
    ...openedMixin(theme),
    '& .MuiDrawer-paper': openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    '& .MuiDrawer-paper': closedMixin(theme),
  }),
}));

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: 'flex',
    height: '100%',
    width: 'auto',
  },
  mobileContainer: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
    color: AcnColorTheme.darkNavy,
  },
  mobileLogo: {
    position: 'absolute',
    left: 0,
    right: 0,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: theme.spacing(0.5),
  },
  drawer: {
    '& .MuiPaper-root': {
      marginTop: theme.spacing(7),
    },
  },
}));

interface AcnLeftMenuProps {
  children: React.ReactElement;
}

const AcnLeftMenu: FC<AcnLeftMenuProps> = (props) => {
  const { children } = props;
  const location = useLocation();
  const navigate = useNavigate();
  const theme = useTheme();
  const classes = useStyles();
  const isLargerThanMobile = useMediaQuery(theme.breakpoints.up('sm'));
  const [open, setOpen] = useState(isLargerThanMobile);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(updateIsLeftMenuOpen(isLargerThanMobile));
    // getUserData();
  }, [dispatch, isLargerThanMobile]);

  const itemColor = (pathName: string) => {
    if (pathName === '/') return location.pathname === pathName ? AcnColorTheme.primary : theme.palette.text.primary;
    return location.pathname.startsWith(pathName) ? AcnColorTheme.primary : theme.palette.text.primary;
  };

  const handleDrawer = () => {
    dispatch(updateIsLeftMenuOpen(!open));
    setOpen(!open);
  };

  if (checkAuthPath(location.pathname)) return children;
  return (
    <>
      <Hidden smDown key="desktop-version">
        <StyledDrawer variant="permanent" open={open}>
          <DrawerHeader
            sx={{
              justifyContent: open ? 'space-between' : 'center',
              paddingRight: open ? theme.spacing(3) : theme.spacing(4),
            }}
          >
            {open && (
              <a rel="noopener noreferrer" href={window.chaiyoGlobalConfig.websiteUrl} target="_blank">
                <img alt="logo" src={logo} />
              </a>
            )}
            <IconButton onClick={handleDrawer}>
              {open ? (
                <ChevronLeftIcon
                  sx={{
                    color: AcnColorTheme.blue100,
                  }}
                  fontSize="large"
                />
              ) : (
                <ChevronRightIcon
                  sx={{
                    color: AcnColorTheme.blue100,
                  }}
                  fontSize="large"
                />
              )}
            </IconButton>
          </DrawerHeader>

          <ListMenu navigate={navigate} location={location} itemColor={itemColor} open={open} />
          <DrawerHeader
            sx={{
              justifyContent: open ? 'space-between' : 'center',
              paddingRight: open ? theme.spacing(3) : theme.spacing(4),
            }}
          >
            {open ? (
              <Box style={{ overflow: 'hidden', marginBottom: 12, marginLeft: 16 }}>
                <AcnAppVersion color="gray" size={12} />
              </Box>
            ) : (
              <Box />
            )}
          </DrawerHeader>
        </StyledDrawer>
      </Hidden>
      <Hidden smUp key="mobile-version">
        <AppBar position="fixed" sx={{ height: 'fit-content', boxShadow: 'none', zIndex: theme.zIndex.drawer + 1 }}>
          <Toolbar style={{ backgroundColor: 'white' }}>
            <div className={classes.mobileContainer}>
              <IconButton
                size="large"
                edge="start"
                color="inherit"
                aria-label="menu"
                sx={{ mr: 2 }}
                onClick={handleDrawer}
              >
                {open ? <ChevronLeftIcon /> : <NotesIcon />}
              </IconButton>
              <a rel="noopener noreferrer" href={window.chaiyoGlobalConfig.websiteUrl} target="_blank">
                <img alt="mobiel_logo" className={classes.mobileLogo} src={logo} />
              </a>
            </div>
          </Toolbar>
        </AppBar>
        <Drawer className={classes.drawer} open={open} onClose={handleDrawer}>
          <ListMenu navigate={navigate} location={location} itemColor={itemColor} open={open} />
          <DrawerHeader
            sx={{
              justifyContent: open ? 'space-between' : 'center',
              paddingRight: open ? theme.spacing(3) : theme.spacing(4),
              paddingBottom: 8,
            }}
          >
            <Box style={{ overflow: 'hidden' }}>
              <AcnAppVersion color="gray" size={12} />
            </Box>
          </DrawerHeader>
        </Drawer>
        <Toolbar />
      </Hidden>
      <Box component="main" sx={{ flexGrow: 1, p: 3, overflow: 'hidden', padding: 0 }}>
        {children}
      </Box>
    </>
  );
};

export default AcnLeftMenu;
