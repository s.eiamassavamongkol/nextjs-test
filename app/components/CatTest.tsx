import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";

async function fetchCatFacts() {
  const res = await fetch("https://cat-fact.herokuapp.com/facts");
  if (!res.ok) {
    throw new Error("Failed to fetch");
  }
  const data = await res.json();
  return data;
}

const CatFacts: React.FC = () => {
  const [data, setData] = useState<any[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const getData = async () => {
      try {
        const facts = await fetchCatFacts();
        setData(facts);
      } catch (err: any) {
        setError(err.message);
      } finally {
        setLoading(false);
      }
    };

    getData();
  }, []);

  if (loading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>Error: {error}</p>;
  }

  return (
    <Box>
      <h1>Cat Facts</h1>
      <ul>
        {data.map((fact) => (
          <li key={fact._id}>
            <p>{fact.text}</p>
            <small>
              <strong>Created At:</strong> {new Date(fact.createdAt).toLocaleDateString()}
            </small>
            <br />
            <small>
              <strong>Verified:</strong> {fact.status.verified ? "Yes" : "No"}
            </small>
            <br />
            <small>
              <strong>Used:</strong> {fact.used ? "Yes" : "No"}
            </small>
          </li>
        ))}
      </ul>
    </Box>
  );
};

export default CatFacts;
