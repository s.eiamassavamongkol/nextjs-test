import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import AssignmentOutlinedIcon from '@mui/icons-material/AssignmentOutlined';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import UploadOutlinedIcon from '@mui/icons-material/UploadOutlined';
import LogoutIcon from '@mui/icons-material/Logout';
import SupervisorAccountOutlinedIcon from '@mui/icons-material/SupervisorAccountOutlined';
import { List, ListItem, ListItemButton, Theme, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React, { FC, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { Location } from 'react-router-dom';
import { MainPath, TaskPath, AgentInformationPath } from '../../constant';
import { useMountEffect } from '../../hooks';
import { clearStoredValue } from '../../utils';
import { AcnColorTheme } from '../theme';
import AcnBackdrop from '../backdrop/AcnBackdrop';
import useCommonDialog from '../../hooks/useCommonDialog';
import AcnCommonDialog from '../dialog/AcnCommonDialog';
import UserPool from '../../utils/cognito/constant/UserPool';
import { signOut } from '../../utils/cognito/Signout';

interface ListMenuProps {
  navigate: (pathName: string) => void;
  location: Location;
  itemColor: (pathName: string) => string;
  open: boolean;
}

const useStyles = makeStyles<Theme, ListMenuProps>((theme) => ({
  list: {
    width: ({ open }) => (open ? 'auto' : theme.spacing(15)),
    [theme.breakpoints.down('sm')]: {
      width: '75vw',
      height: `calc(100vh - ${theme.spacing(11)})`,
    },
    border: `1px solid ${AcnColorTheme.disabled}`,
    borderTop: 'none',
    borderBottom: 'none',
    borderRight: 'none',
    '& .MuiListItem-root:first-child': {
      display: 'flex',
      justifyContent: 'space-between',
      padding: theme.spacing(3, 4, 1.5),
      [theme.breakpoints.down('sm')]: {
        padding: theme.spacing(0, 2),
      },
    },
    '& .MuiListItem-root:last-child': {
      [theme.breakpoints.down('sm')]: {
        position: 'absolute',
        bottom: 0,
      },
    },
    '& .MuiListItem-root': {
      padding: theme.spacing(0.5, 4),
      [theme.breakpoints.down('sm')]: {
        padding: theme.spacing(0, 2),
      },
    },
  },
  listItem: {
    '& .MuiListItemButton-root': {
      borderRadius: theme.spacing(1),
      padding: theme.spacing(2),
      [theme.breakpoints.down('sm')]: {
        padding: theme.spacing(1.5),
      },
    },
    '& .Mui-selected': {
      border: '1px solid #B8BEFF',
    },
  },
}));

const ListMenu: FC<ListMenuProps> = (props) => {
  const { location, navigate, itemColor, open } = props;
  // const [role, setRole] = useState('');
  const dispatch = useDispatch();
  const classes = useStyles(props);
  const { t } = useTranslation();
  const { isOpen, dialogProps, displayCommonDialog, hideCommonDialog } = useCommonDialog();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  // const [openPromptDialog, setOpenPromptDialog] = useState(false);
  // const [nextLocation, setNextLocation] = useState<string>('');

  // const getRole = async () => {
  //   const value = await getUserRole();
  //   setRole(value);
  // };
  useMountEffect(() => {
    // getRole();
  });

  const items = [
    {
      label: 'left_menu.home',
      path: MainPath.HOME,
      icon: HomeOutlinedIcon,
    },
    { label: 'left_menu.report', path: TaskPath.TASK, icon: AssignmentOutlinedIcon },
    {
      label: 'left_menu.agent_infomation',
      path: AgentInformationPath.AGENT_INFORMATION,
      icon: SupervisorAccountOutlinedIcon,
    },
    {
      label: 'left_menu.upload_file',
      path: MainPath.UPLOAD_AGENT_FILE,
      icon: UploadOutlinedIcon,
    },
    {
      label: 'left_menu.profile',
      path: MainPath.PROFILE_INFOMATION,
      icon: AccountCircleOutlinedIcon,
    },
    { label: 'left_menu.logout', path: 'log-out', icon: LogoutIcon },
  ];

  // const [isDialog1Open, setIsDialog1Open] = useState(false);
  const handleShowDialogLogout = () => {
    displayCommonDialog({
      id: 'logout-dialog',
      primaryLabel: 'อยู่ต่อ',
      secondaryLabel: 'ออกจากระบบ',
      title: 'ออกจากระบบ',
      message: 'คุณต้องการออกจากระบบใช่หรือไม่',
      onSecondaryAction: () => {
        onSubmit();
      },
    });
  };

  // const handleDialog1 = () => {
  //   setIsDialog1Open(!isDialog1Open);
  // };
  const onSubmit = () => {
    const id = 'logout';
    setIsLoading(true);
    const cognitoUser = UserPool.getCurrentUser();
    cognitoUser?.getSession(async (err: any, session: any) => {
      if (err) {
        clearStoredValue();
        dispatch({ type: 'LOGOUT' });
        setIsLoading(false);
        // setIsDialog1Open(!isDialog1Open);
      }
      try {
        await signOut();
        navigate(MainPath.LOGIN);
        hideCommonDialog();
      } catch (error) {
        hideCommonDialog();
        dispatch({ type: 'LOGOUT' });
        clearStoredValue();
        navigate(MainPath.LOGIN);
      } finally {
        setIsLoading(false);
      }
    });
  };

  const handleOnClickMenu = (path: string) => {
    if (path === 'log-out') handleShowDialogLogout();
    // else if (dropOffPromptPaths.includes(location.pathname)) {
    //   setNextLocation(path);
    //   setOpenPromptDialog(true);
    // }
    else {
      navigate(path);
    }
  };

  // const confirmNavigation = () => {
  //   setOpenPromptDialog(false);
  //   navigate(nextLocation);
  // };

  return (
    <List className={classes.list}>
      {/* <AcnDialog
        showCloseButton
        title="ออกจากระบบ"
        message="คุณต้องการออกจากระบบใช่หรือไม่"
        primaryLabel="อยู่ต่อ"
        secondaryLabel="ออกจากระบบ"
        onPrimaryAction={handleDialog1}
        onSecondaryAction={onSubmit}
        onClose={handleDialog1}
        open={isDialog1Open}
      /> */}
      {/* <AcnDialog
        title="ออกจากหน้านี้"
        message="คุณต้องการออกจากหน้านี้ใช่หรือไม่"
        primaryLabel="อยู่ต่อ"
        secondaryLabel="ออกจากหน้านี้"
        onPrimaryAction={() => setOpenPromptDialog(false)}
        onSecondaryAction={confirmNavigation}
        open={openPromptDialog}
      /> */}
      {items.map((item) => {
        const Icon = item.icon;
        return (
          <ListItem key={item.label} className={classes.listItem} disablePadding>
            <ListItemButton
              selected={item.path === '/' ? location.pathname === item.path : location.pathname.includes(item.path)}
              onClick={() => handleOnClickMenu(item.path)}
              // disabled={item.disable}
            >
              <Icon
                style={{ color: itemColor(item.path), fontSize: open ? '33px' : '24px' }}
                sx={{ paddingRight: open ? (theme) => theme.spacing(1.25) : 0 }}
              />
              {open && (
                <Typography variant="content2" style={{ color: itemColor(item.path) }}>
                  {t(item.label)}
                </Typography>
              )}
            </ListItemButton>
          </ListItem>
        );
      })}
      <AcnBackdrop open={isLoading} />
      <AcnCommonDialog isOpen={isOpen} dialogProps={dialogProps} />
    </List>
  );
};

export default React.memo(ListMenu);
